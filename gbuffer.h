#pragma once

#include "gl.h"
#include "rendertarget.h"

enum GBufferMask {
	COLOR=1,		// r,g,b,a
	NORMAL=2,		// nx,ny,nz,-
	POSITION=4,		// x,y,z,-
	DEPTH=8,		// 1/z
	PHONG=16		// ambient [0..1], diffuse [0..1], specular [0..1], shininess [0..1 -> 0..100]
};

class GBuffer {
private:
	GLuint _color;
	GLuint _normal;
	GLuint _position;
	GLuint _depth;
	GLuint _phong;
	GLsizei _width;
	GLsizei _height;
	RenderTarget* _fbo;

public:
	GBuffer(GLsizei width, GLsizei height, GBufferMask mask);
	virtual ~GBuffer();

	RenderTarget* renderTarget();
	void use();
	GLuint color();
	GLuint normal();
	GLuint position();
	GLuint depth();
	GLuint phong();
	GLsizei width();
	GLsizei height();
};
