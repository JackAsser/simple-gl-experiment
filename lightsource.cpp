#include "lightsource.h"

matrix4 LightSource::projection = perspectiveFOV(M_PI/2, 1, 0.1);

LightSource::LightSource() {
    _fbo = new RenderTarget(64,64,true);

 	_shadowMap = createTexture(GL_TEXTURE_CUBE_MAP, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE, GL_LINEAR, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_COMPARE_FUNC, GL_GEQUAL);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, GL_DEPTH_COMPONENT, _fbo->width(), _fbo->height(), 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, GL_DEPTH_COMPONENT, _fbo->width(), _fbo->height(), 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, GL_DEPTH_COMPONENT, _fbo->width(), _fbo->height(), 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, GL_DEPTH_COMPONENT, _fbo->width(), _fbo->height(), 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, GL_DEPTH_COMPONENT, _fbo->width(), _fbo->height(), 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, GL_DEPTH_COMPONENT, _fbo->width(), _fbo->height(), 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
    _fbo->attachTextureCube(GL_DEPTH_ATTACHMENT, _shadowMap);

    _position = float3(0,0,0);
    _color = float3(1,1,1);
    _intensity = 1.0;
}

LightSource::~LightSource() {
	delete _fbo;
}

void LightSource::setIntensity(float intensity) {
    _intensity = intensity;
}

float LightSource::getIntensity() {
    return _intensity;
}

void LightSource::setPosition(float3 position) {
	_position = position;

    static float3 vx = float3(1,0,0);
    static float3 vy = float3(0,1,0);
    static float3 vz = float3(0,0,1);

    _v[0] = lookAt(_position, _position-vx, vy);
    _v[1] = lookAt(_position, _position+vx, vy);
    _v[2] = lookAt(_position, _position-vy, vz);
    _v[3] = lookAt(_position, _position+vy, -vz);
    _v[4] = lookAt(_position, _position+vz, vy);
    _v[5] = lookAt(_position, _position-vz, vy);
    for (int i=0; i<6; i++)
        _v[i] = _v[i]*projection;
}

float3 LightSource::position() {
	return _position;
}

void LightSource::setColor(float3 color) {
    _color = color;
}

float3 LightSource::getColor() {
    return _color;
}

void LightSource::generateShadowMap(SceneRenderer* renderer) {
	_fbo->use();
    matrix4 i;
	renderer->renderScene(SHADOWMAPS, projection, _v);
}

GLuint LightSource::shadowMap() {
	return _shadowMap;
}
