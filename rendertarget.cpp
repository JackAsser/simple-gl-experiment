#include "rendertarget.h"
#include "util.h"

RenderTarget::RenderTarget(GLsizei width, GLsizei height, bool isFBO) {
	_width = width;
	_height = height;
	_frameBuffer = 0;
	_isFBO = isFBO;
	_nbrAttachments = 0;

	if (isFBO)
		glGenFramebuffers(1, &_frameBuffer);
	else
		registerAttachment(GL_COLOR_ATTACHMENT0);
}

RenderTarget::~RenderTarget() {
}

GLuint RenderTarget::frameBuffer() {
	return _frameBuffer;
}

void RenderTarget::registerAttachment(GLenum attachment) {
	if (attachment == GL_DEPTH_ATTACHMENT)
		return;
	_attachments[_nbrAttachments] = attachment;
	_nbrAttachments++;
}

void RenderTarget::use() {
	glViewport(0,0,_width,_height);
	glBindFramebuffer(GL_FRAMEBUFFER, _frameBuffer);
	glDrawBuffers(_nbrAttachments, _attachments);
}

void RenderTarget::colorOnly() {
	GLenum tmp = GL_COLOR_ATTACHMENT0;
	glDrawBuffers(1, &tmp);	
}

GLsizei RenderTarget::width() {
	return _width;
}

GLsizei RenderTarget::height() {
	return _height;
}

void RenderTarget::validate() {
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		bail("Incomplete FBO when attaching texture. FBO:%d Status:%x", _frameBuffer, glCheckFramebufferStatus(GL_FRAMEBUFFER));
}

void RenderTarget::attachTexture1D(GLenum attachment, GLuint texture, GLint level) {
	if (!_isFBO)
		bail("Can not attach textures to a non FBO! %d",0);
	glBindFramebuffer(GL_FRAMEBUFFER, _frameBuffer);
    glFramebufferTexture1D(GL_FRAMEBUFFER, attachment, GL_TEXTURE_1D, texture, level);
    registerAttachment(attachment);
	validate();
}

void RenderTarget::attachTexture2D(GLenum attachment, GLuint texture, GLint level) {
	if (!_isFBO)
		bail("Can not attach textures to a non FBO! %d",0);
	glBindFramebuffer(GL_FRAMEBUFFER, _frameBuffer);
    glFramebufferTexture2D(GL_FRAMEBUFFER, attachment, GL_TEXTURE_2D, texture, level);
    registerAttachment(attachment);
	validate();
}

void RenderTarget::attachTexture2DMultisample(GLenum attachment, GLuint texture) {
	if (!_isFBO)
		bail("Can not attach textures to a non FBO! %d",0);
	glBindFramebuffer(GL_FRAMEBUFFER, _frameBuffer);
    glFramebufferTexture2D(GL_FRAMEBUFFER, attachment, GL_TEXTURE_2D_MULTISAMPLE, texture, 0);
    registerAttachment(attachment);
	validate();
}

void RenderTarget::attachTexture3D(GLenum attachment, GLuint texture, GLint level, GLint layer) {
	if (!_isFBO)
		bail("Can not attach textures to a non FBO! %d",0);
	glBindFramebuffer(GL_FRAMEBUFFER, _frameBuffer);
    glFramebufferTexture3D(GL_FRAMEBUFFER, attachment, GL_TEXTURE_3D, texture, level, layer);
    registerAttachment(attachment);
	validate();
}

void RenderTarget::attachTextureCube(GLenum attachment, GLuint texture) {
	if (!_isFBO)
		bail("Can not attach textures to a non FBO! %d",0);
	glBindFramebuffer(GL_FRAMEBUFFER, _frameBuffer);
    glFramebufferTexture(GL_FRAMEBUFFER, attachment, texture, 0);
    registerAttachment(attachment);
	validate();
}

