#pragma once

#include "program.h"
#include "rendertarget.h"
#include "scenerenderer.h"
#include "lightsource.h"
#include "gbuffer.h"
#include "proceduraltexture.h"
#include <vector>

class Engine : public SceneRenderer {
private:
	GBuffer* gBuffer;
	GBuffer* ssaoBuffer;
	GBuffer* accumulateBuffer;

	Program *dof;
	Program *visualizeCube;
	Program *visualizeQuad;
	Program *simple;
	Program *shadowMapProgram;
	Program *whiteProgram;
	Program *ssaoProgram;
	Program *ambientProgram;
	Program *omnilightProgram;

	ProceduralTexture* shaderToyTexture;
	ProceduralTexture* checkerboardTexture;
	ProceduralTexture* plainColorTexture;
	ProceduralTexture* randomTexture;
	
	std::vector<LightSource*> lightSources;

	double _time;
	double _rotX;
	double _rotY;

protected:
	void renderScene(RenderPass renderPass, matrix4 projection, matrix4* view);

public:
	Engine();
	virtual ~Engine();
	void init(RenderTarget& frontBuffer);
	void render(RenderTarget& frontBuffer);
	void handleMouse(double dx, double dy);
};
