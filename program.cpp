#include "program.h"
#include "util.h"

using namespace std;

GLuint Program::quadVBO = (GLuint)0xffffffff;
GLuint Program::cubeVBO = (GLuint)0xffffffff;
GLuint Program::cubeIndexVBO = (GLuint)0xffffffff;
GLuint Program::cubeTextureVBO = (GLuint)0xffffffff;

Program::Program(string name) {
	_name = name;
	_index = glCreateProgram();
	_quadVAO = (GLuint)0xffffffff;
	_cubeVAO = (GLuint)0xffffffff;
	_cubeTextureVAO = (GLuint)0xffffffff;
}

Program::~Program() {

}

void Program::attachShader(Shader& shader) {
	glAttachShader(_index, shader.index());
}

void Program::link() {
    glLinkProgram(_index);

    GLint status;
    glGetProgramiv(_index, GL_LINK_STATUS, &status);
    if (status == GL_FALSE) {
	    GLint logLength;
        glGetProgramiv(_index, GL_INFO_LOG_LENGTH, &logLength);
        if (logLength > 0) {
        	unique_ptr<char[]> log(new char[logLength]); 

            glGetProgramInfoLog(_index, logLength, &logLength, (GLchar*)log.get());
            fprintf(stderr, "%s", log.get());
        }
        bail("Failed to link program '%s'", _name.c_str());
   	}
}

void Program::use() {
    glUseProgram(_index);
}

GLint Program::getUniformLocation(string& name) {
	GLint loc;

	map<string, GLint>::iterator i = _uniformLocations.find(name);
	if (i == _uniformLocations.end()) {
		loc = glGetUniformLocation(_index, name.c_str());
		if (loc == -1)
			fprintf(stderr, "Failed to find uniform '%s' in program '%s'\n", name.c_str(), _name.c_str());
		_uniformLocations[name] = loc;
	}
	else
		loc = i->second;

	return loc;
}

GLint Program::getAttributeLocation(string& name) {
	GLint loc;

	map<string, GLint>::iterator i = _attributeLocations.find(name);
	if (i == _attributeLocations.end()) {
		loc = glGetAttribLocation(_index, name.c_str());
		if (loc == -1)
			fprintf(stderr, "Failed to find attribute '%s' in program '%s'\n", name.c_str(), _name.c_str());
		_attributeLocations[name] = loc;
	}
	else
		loc = i->second;

	return loc;
}

void Program::uniform(string name, float value) {
	GLint loc = getUniformLocation(name);
	if (loc>=0)
		glUniform1f(loc, value);
}

void Program::uniform(string name, double value) {
	GLint loc = getUniformLocation(name);
	if (loc>=0)
		glUniform1f(loc, value);
}

void Program::uniform(string name, int value) {
	GLint loc = getUniformLocation(name);
	if (loc>=0)
		glUniform1i(loc, value);
}

void Program::uniform(string name, float x, float y) {
	GLint loc = getUniformLocation(name);
	if (loc>=0)
		glUniform2f(loc, x, y);
}

void Program::uniform(string name, float3 value) {
	GLint loc = getUniformLocation(name);
	if (loc>=0)
		glUniform3f(loc, value.x(), value.y(), value.z());
}

void Program::uniform(string name, matrix4 value, bool onlyTopLeft) {
	GLint loc = getUniformLocation(name);
	if (loc>=0) {
        float tmp[16];
        value.store(tmp);

        if (onlyTopLeft) {
            float tmp2[9];
            tmp2[0]=tmp[0];
            tmp2[1]=tmp[1];
            tmp2[2]=-tmp[2];
            tmp2[3]=tmp[4];
            tmp2[4]=tmp[5];
            tmp2[5]=-tmp[6];
            tmp2[6]=tmp[8];
            tmp2[7]=tmp[9];
            tmp2[8]=-tmp[10];
            glUniformMatrix3fv(loc, 1, false, tmp2);
        }
        else
            glUniformMatrix4fv(loc, 1, false, tmp);
	}
}

void Program::uniform(string name, int count, matrix4* values) {
    GLint loc = getUniformLocation(name);
    if (loc>=0) {
        float tmp[16*count];
        for (int i=0; i<count; i++)
            values[i].store(tmp+i*16);
        glUniformMatrix4fv(loc, count, false, tmp);
    }   
}


void Program::attribPointer(string name, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid* ptr) {
	GLint loc = getAttributeLocation(name);
	if (loc>=0) {
		glEnableVertexAttribArray(loc);
		glVertexAttribPointer(loc, size, type, normalized, stride, ptr);
	}
}

void Program::drawQuad() {
	static GLfloat quad[] = { //x,y,z,w,u,v
        -1.0,  1.0, 1,1, 0,1,
        -1.0, -1.0, 1,1, 0,0,
         1.0,  1.0, 1,1, 1,1,
         1.0, -1.0, 1,1, 1,0
    };

	if (_quadVAO == 0xffffffff) {
    	glGenVertexArrays(1, &_quadVAO);
    	glBindVertexArray(_quadVAO);

    	if (quadVBO == 0xffffffff) {
    		glGenBuffers(1, &quadVBO);
    		glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
		    glBufferData(GL_ARRAY_BUFFER, sizeof(quad), quad, GL_STATIC_DRAW);
    	}
    	else
    		glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
	
	    attribPointer("inPosition", 4, GL_FLOAT, GL_FALSE, sizeof(GLfloat)*6, (GLvoid*)(sizeof(GLfloat)*0));
    	attribPointer("inTexCoord", 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat)*6, (GLvoid*)(sizeof(GLfloat)*4));
	} else
    	glBindVertexArray(_quadVAO);

	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

void Program::drawCube() {
	static GLfloat cube[] = { //x,y,z,nx,ny,nz,tx,ty,tz,u,v
		// front
		-1, -1,  1, 0,0,1, 0,1,0, 0,1,
         1, -1,  1, 0,0,1, 0,1,0, 0,0,
         1,  1,  1, 0,0,1, 0,1,0, 1,0,
        -1,  1,  1, 0,0,1, 0,1,0, 1,1,
        // right
		 1,  1,  1,  1,0,0, 0,1,0, 1,1,
         1,  1, -1,  1,0,0, 0,1,0, 1,0,
         1, -1, -1,  1,0,0, 0,1,0, 0,0,
         1, -1,  1,  1,0,0, 0,1,0, 0,1,
        // back
		-1, -1, -1,  0,0,-1, 0,1,0, 0,0,
         1, -1, -1,  0,0,-1, 0,1,0, 0,1,
         1,  1, -1,  0,0,-1, 0,1,0, 1,1,
        -1,  1, -1,  0,0,-1, 0,1,0, 1,0,
        // left
		-1, -1, -1, -1,0,0, 0,1,0, 0,1,
        -1, -1,  1, -1,0,0, 0,1,0, 0,0,
        -1,  1,  1, -1,0,0, 0,1,0, 1,0,
        -1,  1, -1, -1,0,0, 0,1,0, 1,1,
        // top
		 1,  1,  1, 0,1,0, 1,0,0, 1,0,
        -1,  1,  1, 0,1,0, 1,0,0, 0,0,
        -1,  1, -1, 0,1,0, 1,0,0, 0,1,
         1,  1, -1, 0,1,0, 1,0,0, 1,1,
         // bottom
		-1, -1, -1, 0,-1,0, 1,0,0, 0,0,
         1, -1, -1, 0,-1,0, 1,0,0, 1,0,
         1, -1,  1, 0,-1,0, 1,0,0, 1,1,
        -1, -1,  1, 0,-1,0, 1,0,0, 0,1,
    };

    static GLuint cubeIndices[] = {
    	0,  1,  2,  0,  2,  3,   //front
		4,  6,  5,  4,  7,  6,   //right
		8,  10, 9,  8,  11, 10,  //back
		12, 13, 14, 12, 14, 15,  //left
		16, 18, 17, 16, 19, 18,  //upper
		20, 21, 22, 20, 22, 23   //bottom
    };

	if (_cubeVAO == 0xffffffff) {
    	glGenVertexArrays(1, &_cubeVAO);
    	glBindVertexArray(_cubeVAO);

    	if (cubeVBO == 0xffffffff) {
    		glGenBuffers(1, &cubeVBO);
    		glBindBuffer(GL_ARRAY_BUFFER, cubeVBO);
		    glBufferData(GL_ARRAY_BUFFER, sizeof(cube), cube, GL_STATIC_DRAW);

    		glGenBuffers(1, &cubeIndexVBO);
    		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, cubeIndexVBO);
		    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(cubeIndices), cubeIndices, GL_STATIC_DRAW);
    	}
    	else {
    		glBindBuffer(GL_ARRAY_BUFFER, cubeVBO);
    		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, cubeIndexVBO);
    	}
	
	    attribPointer("inPosition", 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat)*11, (GLvoid*)(sizeof(GLfloat)*0));
    	attribPointer("inNormal", 3, GL_FLOAT, GL_TRUE, sizeof(GLfloat)*11, (GLvoid*)(sizeof(GLfloat)*3));
        attribPointer("inTangent", 3, GL_FLOAT, GL_TRUE, sizeof(GLfloat)*11, (GLvoid*)(sizeof(GLfloat)*6));
    	attribPointer("inTexCoord", 2, GL_FLOAT, GL_TRUE, sizeof(GLfloat)*11, (GLvoid*)(sizeof(GLfloat)*9));
	} else
    	glBindVertexArray(_cubeVAO);

	glDrawElements(GL_TRIANGLES, 2*3*6, GL_UNSIGNED_INT, (void*)0);
}

void Program::drawCubeTexture() {
    static GLfloat xs = 1.0f/4.0f;
    static GLfloat ys = 1.0f/3.0f;
	static GLfloat cubeFaces[] = { //x,y,z,u,v,w
		 // negx
        -1*xs-3*xs,  1*ys, 0, -1, 1, 1,
        -1*xs-3*xs, -1*ys, 0, -1,-1, 1,
         1*xs-3*xs,  1*ys, 0, -1, 1,-1,
         1*xs-3*xs, -1*ys, 0, -1,-1,-1,
        
        // negz
         1*xs-1*xs,  1*ys, 0,  1, 1,-1,
         1*xs-1*xs, -1*ys, 0,  1,-1,-1,
    
        // posx
         1*xs+1*xs,  1*ys, 0,  1, 1, 1,
         1*xs+1*xs, -1*ys, 0,  1,-1, 1,
    
        // posz
         1*xs+3*xs,  1*ys, 0, -1, 1, 1,
         1*xs+3*xs, -1*ys, 0, -1,-1, 1,
        
        // posy
        -1*xs-1*xs,  1*ys+2*ys, 0, -1, 1, 1,
        -1*xs-1*xs, -1*ys+2*ys, 0, -1, 1,-1,
         1*xs-1*xs,  1*ys+2*ys, 0,  1, 1, 1,
         1*xs-1*xs, -1*ys+2*ys, 0,  1, 1,-1,
        
        // negy
        -1*xs-1*xs,  1*ys-2*ys, 0, -1,-1,-1,
        -1*xs-1*xs, -1*ys-2*ys, 0, -1,-1, 1,
         1*xs-1*xs,  1*ys-2*ys, 0,  1,-1,-1,
         1*xs-1*xs, -1*ys-2*ys, 0,  1,-1, 1,
    };

	if (_cubeTextureVAO == 0xffffffff) {
    	glGenVertexArrays(1, &_cubeTextureVAO);
    	glBindVertexArray(_cubeTextureVAO);

    	if (cubeTextureVBO == 0xffffffff) {
    		glGenBuffers(1, &cubeTextureVBO);
    		glBindBuffer(GL_ARRAY_BUFFER, cubeTextureVBO);
		    glBufferData(GL_ARRAY_BUFFER, sizeof(cubeFaces), cubeFaces, GL_STATIC_DRAW);
    	}
    	else
    		glBindBuffer(GL_ARRAY_BUFFER, cubeTextureVBO);
	
	    attribPointer("inPosition", 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat)*6, (GLvoid*)(sizeof(GLfloat)*0));
    	attribPointer("inTexCoord", 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat)*6, (GLvoid*)(sizeof(GLfloat)*3));
	} else
    	glBindVertexArray(_cubeTextureVAO);

    glDrawArrays(GL_TRIANGLE_STRIP,0,10);
    glDrawArrays(GL_TRIANGLE_STRIP,10,4);
    glDrawArrays(GL_TRIANGLE_STRIP,14,4);
}
