ARCH=x86_64
MIN_OS=10.11
LIBS=-lGLEW -lglfw3 -lSystem -lc++ -lstdc++
FRAMEWORKS=-framework OpenGL -framework Cocoa -framework IOKit -framework CoreVideo
INCLUDES=-I/usr/local/include/

LDFLAGS=-arch $(ARCH) -macosx_version_min $(MIN_OS) $(FRAMEWORKS) $(LIBS)
CXXFLAGS=-g -arch $(ARCH) -mmacosx-version-min=$(MIN_OS) -std=c++11 -stdlib=libc++ -Wall -Wimplicit-function-declaration $(INCLUDES)

SRCS=main.cpp engine.cpp shader.cpp program.cpp rendertarget.cpp gl.cpp lightsource.cpp gbuffer.cpp proceduraltexture.cpp

quick:
	$(MAKE) -j 8 run

run: main
	./main

depend:
	makedepend $(INCLUDES) $(SRCS) 2> /dev/null

main: $(SRCS:%.cpp=%.o)
	$(LD) -o $@ $^ $(LDFLAGS)

clean:
	rm -rf *.o main *~
	makedepend
	
# DO NOT DELETE
