#pragma once

#include "gl.h"
#include <set>

class RenderTarget {
private:
	GLenum _attachments[10];
	int _nbrAttachments;
	void validate();
	void registerAttachment(GLenum attachment);

protected:
	GLsizei _width;
	GLsizei _height;
	GLuint _frameBuffer;
	bool _isFBO;

public:
	RenderTarget(GLsizei width, GLsizei height, bool isFBO=false);
	virtual ~RenderTarget();
	void use();
	void colorOnly();
	GLuint frameBuffer();
	GLsizei width();
	GLsizei height();
	void attachTexture1D(GLenum attachment, GLuint texture, GLint level=0);
	void attachTexture2D(GLenum attachment, GLuint texture, GLint level=0);
	void attachTexture2DMultisample(GLenum attachment, GLuint texture);
	void attachTexture3D(GLenum attachment, GLuint texture, GLint level=0, GLint layer=0);
	void attachTextureCube(GLenum attachment, GLuint texture);
};
