#version 410

uniform vec2 iResolution;

layout(location=0) out vec3 fragColor;

#define HASHSCALE3 vec3(443.897, 441.423, 437.195)

vec2 hash22(vec2 p) {
	vec3 p3 = fract(vec3(p.xyx) * HASHSCALE3);
    p3 += dot(p3, p3.yzx+19.19);
    return fract(vec2((p3.x + p3.y)*p3.z, (p3.x+p3.z)*p3.y));
}

void main() {
	vec2 uv = gl_FragCoord.xy / iResolution.xy;
	vec3 normal = vec3(hash22(uv), 1.0);
	normal-=0.5;
	normal*=2.0;
	normal.xy*=0.4;
	normal = normalize(normal);

	fragColor = normal*0.5+0.5;
}
