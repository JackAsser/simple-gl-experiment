#version 410

uniform mat4 iMV;
uniform mat4 iP;
uniform float iNormalFlip=1.0;

out vData {
	vec3 normal;
	vec3 tangent;
	vec3 position;
	vec2 texCoord;
} vertex;

in vec3 inPosition;
in vec3 inNormal;
in vec3 inTangent;
in vec2 inTexCoord;

void main(void) {
	vertex.normal = normalize((iMV*vec4(inNormal*iNormalFlip, 0.0)).xyz);
	vertex.tangent = normalize((iMV*vec4(inTangent, 0.0)).xyz);
	vertex.texCoord = inTexCoord;
	vec4 position = iMV*vec4(inPosition, 1.0);
	vertex.position = position.xyz;
	gl_Position = iP*position;
}
