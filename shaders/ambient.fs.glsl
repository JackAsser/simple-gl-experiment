#version 410

uniform sampler2D iColor;
uniform sampler2D iSSAO;

in vec2 texCoord;
out vec4 fragColor;

void main(void) {
	fragColor = vec4(texture(iColor, texCoord).rgb*texture(iSSAO, texCoord).rgb, 1.0);
}
