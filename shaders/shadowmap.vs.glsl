#version 410

uniform mat4 iM;

in vec3 inPosition;

void main(void) {
	gl_Position = iM*vec4(inPosition, 1.0);
}
