#version 410

uniform sampler2D iTexture0;
uniform sampler2D iTexture1;

in vec2 texCoord;
out vec4 fragColor;

const vec2 res = vec2(800.0, 600.0);
const float GA =2.399; 
const mat2 rot = mat2(cos(GA),sin(GA),-sin(GA),cos(GA));

// 	simplyfied version of Dave Hoskins blur
vec3 dof(sampler2D tex, vec2 uv, float rad) {
	vec3 acc=vec3(0);
    vec2 pixel=vec2(.002*res.y/res.x,.002),angle=vec2(0,rad);;
    rad=1.;
	for (int j=0;j<80;j++) {  
        rad += 1./rad;
	    angle*=rot;
        vec4 col=texture(tex, uv+pixel*(rad-1.)*angle);
		acc+=col.xyz;
	}
	return acc/80.;
}

void main(void) {
	float d = (120+texture(iTexture1, texCoord).z);
	fragColor = vec4(dof(iTexture0, texCoord, abs(d)*0.01), 1.0);
}
