#version 410

in vec3 texCoord;
uniform samplerCube iTexture0;
out vec4 fragColor;

void main() {
    fragColor = vec4(texture(iTexture0, texCoord));
}
