#version 410

uniform sampler2D iColor;
uniform sampler2D iNormal;
uniform sampler2D iPosition;
uniform samplerCubeShadow iShadowMap;
uniform vec3 lightPositionVS;
uniform mat3 iInvViewRot;
uniform vec3 lightColor;

in vec2 texCoord;
out vec3 fragColor;

// Near Clip Plane for the shadow map projections
#define NCP_LIGHT 0.1
#define BIAS 0.0007
#define PCF_SCALE 2

void main(void) {
	// Some house keeping
	vec3 positionVS = texture(iPosition, texCoord).xyz;
	vec3 lightPositionVS = lightPositionVS-positionVS;
	vec3 L = normalize(lightPositionVS);
	vec3 normal = normalize(texture(iNormal, texCoord).xyz*2.0-1.0);

	// Calculate the diffuse term in phong
	float diffuseAngle = max(0.0, dot(L, normal));

	// Calculate shadow mapping
	vec3 lightPositionWS = iInvViewRot*lightPositionVS;
	vec3 tmp = abs(lightPositionWS);
	float depth = NCP_LIGHT/max(tmp.x, max(tmp.y, tmp.z));
	vec4 comparator = vec4(lightPositionWS, (depth+1)*0.5+BIAS);
	float shadowIntensity = texture(iShadowMap, comparator);
#if PCF_SCALE > 0
	shadowIntensity += texture(iShadowMap, comparator+vec4(-PCF_SCALE,-PCF_SCALE,-PCF_SCALE,0));
	shadowIntensity += texture(iShadowMap, comparator+vec4( PCF_SCALE,-PCF_SCALE,-PCF_SCALE,0));
	shadowIntensity += texture(iShadowMap, comparator+vec4( PCF_SCALE, PCF_SCALE,-PCF_SCALE,0));
	shadowIntensity += texture(iShadowMap, comparator+vec4(-PCF_SCALE, PCF_SCALE,-PCF_SCALE,0));
	shadowIntensity += texture(iShadowMap, comparator+vec4(-PCF_SCALE,-PCF_SCALE, PCF_SCALE,0));
	shadowIntensity += texture(iShadowMap, comparator+vec4( PCF_SCALE,-PCF_SCALE, PCF_SCALE,0));
	shadowIntensity += texture(iShadowMap, comparator+vec4( PCF_SCALE, PCF_SCALE, PCF_SCALE,0));
	shadowIntensity += texture(iShadowMap, comparator+vec4(-PCF_SCALE, PCF_SCALE, PCF_SCALE,0));
	shadowIntensity /= 9;
#endif

	// Calculate the reflective term in phong
	vec3 R = reflect(L, normal);
	float reflectionAngle = max(0.0, dot(R, normalize(positionVS)));

	// Emit sample according to phong
	fragColor = shadowIntensity * lightColor * (
					0.9 * diffuseAngle * texture(iColor, texCoord).rgb +
					1.0 * pow(reflectionAngle, 15.0)
	);
}
