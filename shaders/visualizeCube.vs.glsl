#version 410

out vec3 texCoord;
in vec3 inPosition;
in vec3 inTexCoord;

void main(void) {
	texCoord = inTexCoord;
	gl_Position = vec4(inPosition,1.0);
}
