#version 410

uniform sampler2D iTexture0;

in vec2 texCoord;
out vec4 fragColor;

void main(void) {
	fragColor = vec4(texture(iTexture0, texCoord).rgb, 1.0);
}
