#version 410

uniform vec2 iResolution;

layout(location=0) out vec3 fragColor;
layout(location=1) out vec3 fragNormal;

void main() {
	vec2 uv = gl_FragCoord.xy / iResolution.xy;
    bool even = mod(floor(uv.x*8) + floor(uv.y*8), 2)==0;
	fragColor = even ? vec3(0.2) : vec3(0.8);
//    fragColor = min(uv.x,uv.y)<0.01 ? vec3(0.5) : fragColor;
  //  fragColor = max(uv.x,uv.y)>0.99 ? vec3(0.5) : fragColor;
    /*
    if (uv.x<0.1)
    	fragColor = vec3(1,0,0);
    if (uv.y<0.1)
    	fragColor = vec3(0,1,0);
    if (min(uv.x, uv.y) > 0.9)
    	fragColor = vec3(0,0,1);
    if (max(uv.x, uv.y) < 0.1)
    	fragColor = vec3(1,1,0);
*/
    vec2 p = mod(uv*8, vec2(1))*2-1;
    p.y=-p.y;
    vec3 n = ((length(p)>0.9)||even) ? vec3(0,0,1) : normalize(vec3(-p*0.8,1));
	fragNormal = n*0.5+0.5;

//    fragColor = vec3(0.2);
  //  fragNormal = vec3(0,0,1);
}
