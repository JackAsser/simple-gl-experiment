#version 410

layout (triangles, invocations=6) in;
layout (triangle_strip, max_vertices=18) out;

uniform mat4 iVPs[6];

void main(void) {
	gl_Layer=gl_InvocationID;
	for (int i=0; i<3; i++) {
		gl_Position = iVPs[gl_InvocationID] * gl_in[i].gl_Position;
		EmitVertex();
    }
    EndPrimitive();
}
