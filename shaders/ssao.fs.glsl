#version 410

uniform sampler2D g_buffer_norm;
uniform sampler2D depth;
uniform sampler2D g_buffer_pos;
uniform sampler2D g_random;
uniform vec3 color;

#define random_size     (450.0/256.0)
#define g_sample_rad    3.0
#define g_intensity     5.0
#define g_scale         20.0
#define g_bias          0.2

in vec2 texCoord;
out vec3 fragColor;

float doAmbientOcclusion(vec2 tcoord, vec2 uv, vec3 p, vec3 cnorm) {
    vec3 diff = texture(g_buffer_pos, tcoord+uv).xyz - p;
    vec3 v = normalize(diff);
    float d = length(diff)*g_scale;
    return max(0.0,dot(cnorm,v)-g_bias)*(1.0/(1.0+d))*g_intensity;
}

#define PERFORM_AO(vec) \
    coord1 = reflect(vec,rand)*rad;\
    tmp = coord1*0.707;\
    coord2 = vec2(tmp.x-tmp.y, tmp.x+tmp.y);\
    ao -= doAmbientOcclusion(texCoord, coord1*0.25, p, n);\
    ao -= doAmbientOcclusion(texCoord, coord2*0.5, p, n);\
    ao -= doAmbientOcclusion(texCoord, coord1*0.75, p, n);\
    ao -= doAmbientOcclusion(texCoord, coord2, p, n);

void main(void) {
    vec3 p = texture(g_buffer_pos, texCoord).xyz;
    vec3 n = normalize(texture(g_buffer_norm, texCoord).xyz*2.0-1.0);
    vec2 rand = normalize(texture(g_random, texCoord*random_size).xy*2.0-1.0);
    float rad = g_sample_rad/p.z;
    
    vec2 coord1, coord2, tmp;
    float ao = 1.0;
    PERFORM_AO(vec2( 1.0,  0.0))
    PERFORM_AO(vec2(-1.0,  0.0))
    PERFORM_AO(vec2( 0.0,  1.0))
    PERFORM_AO(vec2( 0.0, -1.0))
    
    fragColor = color*ao*0.4;
}
