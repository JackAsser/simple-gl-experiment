#version 410

uniform vec2 iResolution;

layout(location=0) out vec3 fragColor;

void main() {
	vec2 uv = gl_FragCoord.xy / iResolution.xy;
	vec2 edge = abs(uv*2-1);
	fragColor = max(edge.x,edge.y)>0.85 ? vec3(1) : vec3(1,0,0);
}
