#version 410

out vec2 texCoord;
in vec4 inPosition;
in vec2 inTexCoord;

void main(void) {
	texCoord = inTexCoord;
	gl_Position = inPosition;
}
