#version 410

uniform sampler2D iColor;
uniform sampler2D iNormal;

in vData {
	vec3 normal;
	vec3 tangent;
	vec3 position;
	vec2 texCoord;
} frag;

layout(location=0) out vec3 fragColor;
layout(location=1) out vec3 fragNormal;
layout(location=2) out vec3 fragPosition;

void main(void) {
	fragColor = texture(iColor, frag.texCoord).rgb;

	vec3 bump = texture(iNormal, frag.texCoord).xyz*2-1;
	mat3 tbn = mat3(frag.tangent, cross(frag.tangent,frag.normal), frag.normal);
	fragNormal = (tbn*bump)*0.5+0.5;
	fragPosition = frag.position;
//	fragColor = texture(iNormal, frag.texCoord).xyz;
}
