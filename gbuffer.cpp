#include "gbuffer.h"

GBuffer::GBuffer(GLsizei width, GLsizei height, GBufferMask mask) {
	_width = width;
	_height = height;

	_fbo = new RenderTarget(width,height,true);
	_color = 0;
	_normal = 0;
	_position = 0;
	_depth = 0;
	_phong = 0;

	if ((mask & COLOR) == COLOR) {
		_color = createTexture(GL_TEXTURE_2D, GL_REPEAT, GL_REPEAT, GL_LINEAR, GL_LINEAR);
    	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_FLOAT, NULL);
    	_fbo->attachTexture2D(GL_COLOR_ATTACHMENT0, _color);	    	
	}

	if ((mask & NORMAL) == NORMAL) {
		_normal = createTexture(GL_TEXTURE_2D, GL_REPEAT, GL_REPEAT, GL_LINEAR, GL_LINEAR);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_FLOAT, NULL);
		_fbo->attachTexture2D(GL_COLOR_ATTACHMENT1, _normal);
	}

	if ((mask & POSITION) == POSITION) {
		_position = createTexture(GL_TEXTURE_2D, GL_REPEAT, GL_REPEAT, GL_LINEAR, GL_LINEAR);
    	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, width, height, 0, GL_RGB, GL_FLOAT, NULL);
    	_fbo->attachTexture2D(GL_COLOR_ATTACHMENT2, _position);
	}

	if ((mask & DEPTH) == DEPTH) {
		_depth = createTexture(GL_TEXTURE_2D, GL_REPEAT, GL_REPEAT, GL_LINEAR, GL_LINEAR);
    	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
    	_fbo->attachTexture2D(GL_DEPTH_ATTACHMENT, _depth);
	}

	if ((mask & PHONG) == PHONG) {
		_phong = createTexture(GL_TEXTURE_2D, GL_REPEAT, GL_REPEAT, GL_LINEAR, GL_LINEAR);
    	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    	_fbo->attachTexture2D(GL_COLOR_ATTACHMENT3, _phong);
	}
}

GBuffer::~GBuffer() {
}

RenderTarget* GBuffer::renderTarget() {
	return _fbo;
}

void GBuffer::use() {
	_fbo->use();
}

GLuint GBuffer::color() {
	return _color;
}
	
GLuint GBuffer::normal() {
	return _normal;	
}
	
GLuint GBuffer::position() {
	return _position;
}
	
GLuint GBuffer::depth() {
	return _depth;	
}

GLuint GBuffer::phong() {
	return _phong;	
}
	
GLsizei GBuffer::width() {
	return _width;
}

GLsizei GBuffer::height() {
	return _height;
}
