#pragma once

#include <string>
#include <map>
#include "gl.h"

class Shader {
private:
	GLuint _index;
	std::string _source;
	static std::map<std::string, GLuint> _cache;

public:
    Shader(const std::string filename, GLenum shaderType);
    virtual ~Shader();
    GLuint index();
    std::string source();
};
