#pragma once

#include "vmath.h"

enum RenderPass {
	CAMERA,
	SHADOWMAPS
};

class SceneRenderer {
public:
	virtual void renderScene(RenderPass renderPass, matrix4 projection, matrix4* view)=0;
};
