#include <stdio.h>
#include <sys/stat.h>
#include "shader.h"
#include "util.h"

using namespace std;

// TODO: Implement shader cache!!!

Shader::Shader(const string filename, GLenum shaderType) {
	struct stat st;
	if (stat(filename.c_str(), &st) != 0)
		bail("Failed to find file '%s'", filename.c_str());

	FILE* f=fopen(filename.c_str(), "r");
	if (f==NULL)
		bail("Failed to open file '%s'", filename.c_str());

	unique_ptr<char[]> buf(new char[st.st_size+1]); 
	fread(buf.get(), st.st_size, 1, f);
	fclose(f);

	buf.get()[st.st_size] = '\0';

	_source = string(buf.get());

	_index = glCreateShader(shaderType);
	
	const GLchar* sourcePointers[1];
	sourcePointers[0] = (GLchar*)_source.c_str();
    glShaderSource(_index, 1, sourcePointers, NULL);
    glCompileShader(_index);
    
    GLint status;
    glGetShaderiv(_index, GL_COMPILE_STATUS, &status);

    if (status == GL_FALSE) {
	    GLint logLength;
        glGetShaderiv(_index, GL_INFO_LOG_LENGTH, &logLength);
        if (logLength > 0) {
        	unique_ptr<char[]> log(new char[logLength]); 

            glGetShaderInfoLog(_index, logLength, &logLength, (GLchar*)log.get());
            fprintf(stderr, "%s", log.get());
        }
        bail("Failed to compile shader '%s'", filename.c_str());
   	}
}

Shader::~Shader() {
}

GLuint Shader::index() {
	return _index;
}

string Shader::source() {
	return _source;
}
