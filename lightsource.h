#pragma once

#include "scenerenderer.h"
#include "vmath.h"
#include "rendertarget.h"
#include "gl.h"

class LightSource {
private:
	float3 _position;
	float3 _color;
	float _intensity;
	RenderTarget* _fbo;
	GLuint _shadowMap;
	matrix4 _v[6];
	static matrix4 projection;

public:
	LightSource();
	virtual ~LightSource();
	void setPosition(float3 position);
	float3 position();
	void setColor(float3 color);
	float3 getColor();
	void setIntensity(float intensity);
	float getIntensity();
	void generateShadowMap(SceneRenderer* renderer);
	GLuint shadowMap();
};
