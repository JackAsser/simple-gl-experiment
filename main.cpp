#include <stdio.h>
#include "gl.h"
#include "engine.h"
#include "rendertarget.h"
#include <string>
#include <execinfo.h>

const GLint WIDTH=800, HEIGHT=600;
Engine e;

void stacktrace() {
    void *array[10];
    size_t size = backtrace(array, 10);
    backtrace_symbols_fd(array, size, fileno(stderr));
}

void keyboardCallback(GLFWwindow *window, int key, int scancode, int action, int mods) {
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GLFW_TRUE);
}

void mouseCallback(GLFWwindow* window, double xpos, double ypos)
{
    static double lastXpos=-1000000;
    static double lastYpos=-1000000;
    if ((lastXpos == -1000000) || (lastYpos == -1000000)) {
        lastXpos = xpos;
        lastYpos = ypos;
        return;
    }

    double dx = xpos-lastXpos;
    double dy = ypos-lastYpos;

    e.handleMouse(dx,dy);

    lastXpos = xpos;
    lastYpos = ypos;
    return;
}

int main(void) {
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

    GLFWwindow *window = glfwCreateWindow(WIDTH, HEIGHT, "Default",NULL/* glfwGetPrimaryMonitor()*/, NULL);
    if (window==NULL) {
        fprintf(stderr, "Failed to create GLFW window\n");
        glfwTerminate();
        return -1;
    }

    GLint screenWidth, screenHeight;
    glfwGetFramebufferSize(window, &screenWidth, &screenHeight);
    
    glfwMakeContextCurrent(window);
    
    glewExperimental = GL_TRUE;
    if (glewInit()!=GLEW_OK) {
        fprintf(stderr, "Failed to initialize GLEW\n");
        return -1;
    }

    glViewport(0, 0, screenWidth, screenHeight);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    glFrontFace(GL_CCW);
    glDepthFunc(GL_GREATER);
    glClearDepth(0);
    glDepthRange(0,1);

    printf("Initialized: OpenGL%s\n", glGetString(GL_VERSION));
    
    glfwSetKeyCallback(window, keyboardCallback);
    glfwSetCursorPosCallback(window, mouseCallback);
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    try {
        RenderTarget frontBuffer(screenWidth, screenHeight);
        frontBuffer.use();
        e.init(frontBuffer);
        while (!glfwWindowShouldClose(window)) {
            glfwPollEvents();
            glBindVertexArray(0);
            glActiveTexture(GL_TEXTURE0);
            frontBuffer.use();
            e.render(frontBuffer);
            glfwSwapBuffers( window );
        }
    } catch (std::string exception) {
        fprintf(stderr, "\nEXCEPTION:\n");
        fprintf(stderr, ">>> %s\n", exception.c_str());
        stacktrace();
    }
    
    glfwTerminate();

    return 0;
}
