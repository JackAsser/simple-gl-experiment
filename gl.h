#pragma once

#define GL4_PROTOTYPES 1
#include <GL/glew.h>
#include <GLFW/glfw3.h>

GLuint createTexture(GLenum target, GLenum wrapS, GLenum wrapT, GLenum magFilter, GLenum minFilter);
