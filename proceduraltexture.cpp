#include "proceduraltexture.h"
#include "program.h"
#include "util.h"

ProceduralTexture::ProceduralTexture(int size, GBufferMask mask, std::string filename) : GBuffer(size, size, mask) {
	p = new Program(string_format("Procedural Texture '%s'", filename.c_str()));
	Shader vs("shaders/quad.vs.glsl", GL_VERTEX_SHADER);
	Shader fs(filename, GL_FRAGMENT_SHADER);
	p->attachShader(vs);
	p->attachShader(fs);
	p->link();
	update();
}

ProceduralTexture::~ProceduralTexture() {
	delete p;
}

void ProceduralTexture::update(double time) {
	use();
	p->use();
    p->uniform("iGlobalTime", time);
    p->uniform("iResolution", width(), height());
    p->uniform("iMouse", 0, 0);
	p->drawQuad();
}

Program* ProceduralTexture::getProgram() {
	return p;
}
