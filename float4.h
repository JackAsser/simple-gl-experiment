#define SHUFFLE4(V, X,Y,Z,W) float4(_mm_shuffle_ps((V).m, (V).m, _MM_SHUFFLE(W,Z,Y,X)))

// A basic float4 class.
// We try to maintain HLSL syntax as much as we can.
struct float4 {
	// Constructors.
	VM_INLINE float4() {}
	VM_INLINE explicit float4(const float *p) { m = _mm_set_ps(p[3], p[2], p[1], p[0]); }
	VM_INLINE explicit float4(float x, float y, float z, float w) { m = _mm_set_ps(w, z, y, x); }
	VM_INLINE explicit float4(__m128 v) { m = v; }
	VM_INLINE explicit float4(float3 v, float w) { m = _mm_set_ps(w, v.z(), v.y(), v.x()); }
	
	// Member accessors.
	// In an ideal world, we'd just use a union and allow the caller to access .x directly.
	// Unfortunately __m128 requires us to write wrappers. This sucks, and breaks HLSL
	// compatibility, but what can you do.
	VM_INLINE float x() const { return _mm_cvtss_f32(m); }
	VM_INLINE float y() const { return _mm_cvtss_f32(_mm_shuffle_ps(m, m, _MM_SHUFFLE(1, 1, 1, 1))); }
	VM_INLINE float z() const { return _mm_cvtss_f32(_mm_shuffle_ps(m, m, _MM_SHUFFLE(2, 2, 2, 2))); }
	VM_INLINE float w() const { return _mm_cvtss_f32(_mm_shuffle_ps(m, m, _MM_SHUFFLE(3, 3, 3, 3))); }
	
	// Unaligned store.
	VM_INLINE void store(float *p) const { p[0] = x(); p[1] = y(); p[2] = z(); p[3] = w(); }

	// Accessors to write to the components directly.
	// Generally speaking you should prefer to construct a new vector, rather than
	// modifiying the components of an existing vector.
	void setX(float x)
	{
		m = _mm_move_ss(m, _mm_set_ss(x));
	}
	void setY(float y)
	{
		__m128 t = _mm_move_ss(m, _mm_set_ss(y));
		t = _mm_shuffle_ps(t, t, _MM_SHUFFLE(3, 2, 0, 0));
		m = _mm_move_ss(t, m);
	}
	void setZ(float z)
	{
		__m128 t = _mm_move_ss(m, _mm_set_ss(z));
		t = _mm_shuffle_ps(t, t, _MM_SHUFFLE(3, 0, 1, 0));
		m = _mm_move_ss(t, m);
	}
	void setW(float w) // TODO: is this correct?!
	{
		__m128 t = _mm_move_ss(m, _mm_set_ss(w));
		t = _mm_shuffle_ps(t, t, _MM_SHUFFLE(3, 0, 0, 0));
		m = _mm_move_ss(t, m);
	}

	VM_INLINE float operator[] (size_t i) const { return m[i]; };
//	VM_INLINE float& operator[] (size_t i) { return m[i]; };

	void dump() {
		printf("[%.3f, %.3f, %.3f, %.3f]\n", m[0], m[1], m[2], m[3]);
	}

	__m128 m;
};

// A small helper to easily load integer arguments without having
// to manually cast everything.
VM_INLINE float4 float4i(int x, int y, int z, int w) { return float4((float)x, (float)y, (float)z, (float)w); }

// Comparison operators need to return a SIMD bool.
// We'll just keep it simple here, but if you want you can actually implement
// this as a separate type with it's own operations.
typedef float4 bool4;

// Basic binary operators.
// Notice that these are all inline. It's not useful to us to ever actually
// step into these in the debugger, even in debug builds.
VM_INLINE float4 operator+ (float4 a, float4 b) { a.m = _mm_add_ps(a.m, b.m); return a; }
VM_INLINE float4 operator- (float4 a, float4 b) { a.m = _mm_sub_ps(a.m, b.m); return a; }
VM_INLINE float4 operator* (float4 a, float4 b) { a.m = _mm_mul_ps(a.m, b.m); return a; }
VM_INLINE float4 operator/ (float4 a, float4 b) { a.m = _mm_div_ps(a.m, b.m); return a; }
VM_INLINE float4 operator* (float4 a, float b) { a.m = _mm_mul_ps(a.m, _mm_set1_ps(b)); return a; }
VM_INLINE float4 operator/ (float4 a, float b) { a.m = _mm_div_ps(a.m, _mm_set1_ps(b)); return a; }
VM_INLINE float4 operator* (float a, float4 b) { b.m = _mm_mul_ps(_mm_set1_ps(a), b.m); return b; }
VM_INLINE float4 operator/ (float a, float4 b) { b.m = _mm_div_ps(_mm_set1_ps(a), b.m); return b; }
VM_INLINE float4& operator+= (float4 &a, float4 b) { a = a + b; return a; }
VM_INLINE float4& operator-= (float4 &a, float4 b) { a = a - b; return a; }
VM_INLINE float4& operator*= (float4 &a, float4 b) { a = a * b; return a; }
VM_INLINE float4& operator/= (float4 &a, float4 b) { a = a / b; return a; }
VM_INLINE float4& operator*= (float4 &a, float b) { a = a * b; return a; }
VM_INLINE float4& operator/= (float4 &a, float b) { a = a / b; return a; }
VM_INLINE bool4 operator==(float4 a, float4 b) { a.m = _mm_cmpeq_ps(a.m, b.m); return a; }
VM_INLINE bool4 operator!=(float4 a, float4 b) { a.m = _mm_cmpneq_ps(a.m, b.m); return a; }
VM_INLINE bool4 operator< (float4 a, float4 b) { a.m = _mm_cmplt_ps(a.m, b.m); return a; }
VM_INLINE bool4 operator> (float4 a, float4 b) { a.m = _mm_cmpgt_ps(a.m, b.m); return a; }
VM_INLINE bool4 operator<=(float4 a, float4 b) { a.m = _mm_cmple_ps(a.m, b.m); return a; }
VM_INLINE bool4 operator>=(float4 a, float4 b) { a.m = _mm_cmpge_ps(a.m, b.m); return a; }
VM_INLINE float4 min(float4 a, float4 b) { a.m = _mm_min_ps(a.m, b.m); return a; }
VM_INLINE float4 max(float4 a, float4 b) { a.m = _mm_max_ps(a.m, b.m); return a; }

// Unary operators.
VM_INLINE float4 operator- (float4 a) { return float4(_mm_setzero_ps()) - a; }
VM_INLINE float4 abs(float4 v) { v.m = _mm_andnot_ps(vsignbits, v.m); return v; }

// Horizontal min/max.
VM_INLINE float hmin(float4 v) {
	v = min(v, SHUFFLE4(v, 1, 0, 2, 3));
	return min(v, SHUFFLE4(v, 2, 0, 1, 3)).x();
}

VM_INLINE float hmax(float4 v) {
	v = max(v, SHUFFLE4(v, 1, 0, 2, 3));
	return max(v, SHUFFLE4(v, 2, 0, 1, 3)).x();
}

// Returns a 4-bit code where bit0..bit3 is X..W
VM_INLINE unsigned mask(float4 v) { return _mm_movemask_ps(v.m) & 15; }

// Once we have a comparison, we can branch based on its results:
VM_INLINE bool any(bool4 v) { return mask(v) != 0; }
VM_INLINE bool all(bool4 v) { return mask(v) == 7; }

// Let's fill out the rest of the HLSL standard libary:
// (there's a few more but this will do to get you started)
VM_INLINE float4 clamp(float4 t, float4 a, float4 b) { return min(max(t, a), b); }
VM_INLINE float sum(float4 v) { return v.x() + v.y() + v.z() + v.w(); }
VM_INLINE float dot(float4 a, float4 b) { return sum(a*b); }
VM_INLINE float length(float4 v) { return sqrtf(dot(v, v)); }
VM_INLINE float lengthSq(float4 v) { return dot(v, v); }
VM_INLINE float4 normalize(float4 v) { return v * (1.0f / length(v)); }
VM_INLINE float4 lerp(float4 a, float4 b, float t) { return a + (b-a)*t; }
