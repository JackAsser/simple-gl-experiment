#include "engine.h"
#include "gl.h"
#include "shader.h"
#include "vmath.h"

static GLuint textures[2];

void Engine::handleMouse(double dx, double dy) {
    _rotX -= dx*0.01;
    _rotY -= dy*0.01;
    if (_rotY < -M_PI/2)
        _rotY = -M_PI/2;
    if (_rotY > M_PI/2)
        _rotY = M_PI/2;
}

void Engine::init(RenderTarget& frontBuffer) {
    _rotX=0;
    _rotY=0;

    float scale = 2;
    gBuffer = new GBuffer(frontBuffer.width()/scale, frontBuffer.height()/scale, (GBufferMask)(COLOR|NORMAL|POSITION|DEPTH));
    ssaoBuffer = new GBuffer(gBuffer->width()/2, gBuffer->height()/2, COLOR);
    accumulateBuffer = new GBuffer(gBuffer->width(), gBuffer->height(), COLOR);
    accumulateBuffer->renderTarget()->attachTexture2D(GL_DEPTH_ATTACHMENT, gBuffer->depth());

    shaderToyTexture = new ProceduralTexture(256, (GBufferMask)(COLOR|NORMAL), "shaders/shadertoy.fs.glsl");
    checkerboardTexture = new ProceduralTexture(1024, (GBufferMask)(COLOR|NORMAL), "shaders/checkerboard.fs.glsl");
    randomTexture = new ProceduralTexture(256, COLOR, "shaders/random.fs.glsl");
    plainColorTexture = new ProceduralTexture(32, COLOR, "shaders/plaincolor.fs.glsl");

/*
    {
        LightSource* l = new LightSource();
        l->setPosition(float3(0,-55,0));
        l->setColor(float3(1,1,1));
        l->setIntensity(1.0);
        lightSources.push_back(l);
    }
*/

    float nbrLightSources = 40;
    for (int i=0; i<nbrLightSources; i++) {
        LightSource* lightSource = new LightSource();
        lightSource->setPosition(float3(0,20,0));
        float v=(M_PI*2.0/nbrLightSources)*((float)i);
        float r=sin(v+M_PI*0.0/3.0)*0.5+0.5;
        float g=sin(v+M_PI*1.0/3.0)*0.5+0.5;
        float b=sin(v+M_PI*2.0/3.0)*0.5+0.5;
        lightSource->setColor(normalize(float3(r,g,b)));
        lightSource->setIntensity(1.0/nbrLightSources);
        lightSources.push_back(lightSource);        
    }
    /*
    lightSource = new LightSource();
    lightSource->setPosition(float3(0,20,0));
    lightSource->setColor(float3(1,0,0));
    lightSources.push_back(lightSource);

    lightSource = new LightSource();
    lightSource->setPosition(float3(-1,20,2));
    lightSource->setColor(float3(0,1,0));
    lightSources.push_back(lightSource);

    lightSource = new LightSource();
    lightSource->setPosition(float3(1,20,2));
    lightSource->setColor(float3(0,0,1));
    lightSources.push_back(lightSource);
*/

    ambientProgram = new Program("Ambient");
    {
        Shader vs("shaders/quad.vs.glsl", GL_VERTEX_SHADER);
        Shader fs("shaders/ambient.fs.glsl", GL_FRAGMENT_SHADER);
        ambientProgram->attachShader(vs);
        ambientProgram->attachShader(fs);
    }
    ambientProgram->link();
    ambientProgram->use();
    ambientProgram->uniform("iColor", 0);
    ambientProgram->uniform("iSSAO", 1);

    omnilightProgram = new Program("Omnilight");
    {
        Shader vs("shaders/quad.vs.glsl", GL_VERTEX_SHADER);
        Shader fs("shaders/omnilight.fs.glsl", GL_FRAGMENT_SHADER);
        omnilightProgram->attachShader(vs);
        omnilightProgram->attachShader(fs);
    }
    omnilightProgram->link();
    omnilightProgram->use();
    omnilightProgram->uniform("iColor", 0);
    omnilightProgram->uniform("iNormal", 1);
    omnilightProgram->uniform("iPosition", 2);
    omnilightProgram->uniform("iShadowMap", 3);

    ssaoProgram = new Program("SSAO");
    {
        Shader vs("shaders/quad.vs.glsl", GL_VERTEX_SHADER);
        Shader fs("shaders/ssao.fs.glsl", GL_FRAGMENT_SHADER);
        ssaoProgram->attachShader(vs);
        ssaoProgram->attachShader(fs);
    }
    ssaoProgram->link();
    ssaoProgram->use();
    ssaoProgram->uniform("g_buffer_norm", 0);
    ssaoProgram->uniform("g_buffer_pos", 1);
    ssaoProgram->uniform("g_random", 2);

    visualizeQuad = new Program("Quad");
    {
        Shader vs("shaders/quad.vs.glsl", GL_VERTEX_SHADER);
        Shader fs("shaders/quad.fs.glsl", GL_FRAGMENT_SHADER);
        visualizeQuad->attachShader(vs);
        visualizeQuad->attachShader(fs);
    }
    visualizeQuad->link();
    visualizeQuad->use();
    visualizeQuad->uniform("iTexture0", 0);

    dof = new Program("Dof");
    {
        Shader vs("shaders/quad.vs.glsl", GL_VERTEX_SHADER);
        Shader fs("shaders/dof.fs.glsl", GL_FRAGMENT_SHADER);
        dof->attachShader(vs);
        dof->attachShader(fs);
    }
    dof->link();
    dof->use();
    dof->uniform("iTexture0", 0);
    dof->uniform("iTexture1", 1);

    simple = new Program("Simple");
    {
        Shader vs("shaders/simple.vs.glsl", GL_VERTEX_SHADER);
        Shader fs("shaders/simple.fs.glsl", GL_FRAGMENT_SHADER);
        simple->attachShader(vs);
        simple->attachShader(fs);
    }
    simple->link();
    simple->use();
    simple->uniform("iColor", 0);
    simple->uniform("iNormal", 1);

    whiteProgram = new Program("White");
    {
        Shader vs("shaders/simple.vs.glsl", GL_VERTEX_SHADER);
        Shader fs("shaders/white.fs.glsl", GL_FRAGMENT_SHADER);
        whiteProgram->attachShader(vs);
        whiteProgram->attachShader(fs);
    }
    whiteProgram->link();

    shadowMapProgram = new Program("ShadowMap");
    {
        Shader vs("shaders/shadowmap.vs.glsl", GL_VERTEX_SHADER);
        Shader gs("shaders/shadowmap.gs.glsl", GL_GEOMETRY_SHADER);
        Shader fs("shaders/shadowmap.fs.glsl", GL_FRAGMENT_SHADER);
        shadowMapProgram->attachShader(vs);
        shadowMapProgram->attachShader(gs);
        shadowMapProgram->attachShader(fs);
    }
    shadowMapProgram->link();

    visualizeCube = new Program("Visualize Cube");
    {
        Shader vs("shaders/visualizeCube.vs.glsl", GL_VERTEX_SHADER);
        Shader fs("shaders/visualizeCube.fs.glsl", GL_FRAGMENT_SHADER);
        visualizeCube->attachShader(vs);
        visualizeCube->attachShader(fs);
    }
    visualizeCube->link();

    glGenTextures(2, textures);
    float pixels[] = {
        0.0f, 0.0f, 0.0f,   1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f,   0.0f, 0.0f, 0.0f
    };

    textures[0] = createTexture(GL_TEXTURE_2D, GL_REPEAT, GL_REPEAT, GL_NEAREST, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 2, 2, 0, GL_RGB, GL_FLOAT, pixels);

    textures[1] = createTexture(GL_TEXTURE_2D, GL_REPEAT, GL_REPEAT, GL_LINEAR, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 2, 2, 0, GL_RGB, GL_FLOAT, pixels);
}

Engine::Engine() {
}

Engine::~Engine() {
}

void Engine::renderScene(RenderPass renderPass, matrix4 projection, matrix4* view) {
    glEnable(GL_DEPTH_TEST);

    Program* program;

    if (renderPass==CAMERA) {
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        program = simple;
        program->use();
        program->uniform("iP", projection);
    } else {
        glClear(GL_DEPTH_BUFFER_BIT);
        program = shadowMapProgram;
        program->use();
        program->uniform("iVPs", 6, view);
    }

    matrix4 model, translate;

    model.scale(float3(-60,-60,-60));
    matrix4 r;
    r.translate(float3(0,30,0));
    model = model*r;

    r.rotZ(-_time*0.2);
  //  model = model*r;
    program->uniform("iNormalFlip", -1.0f);
    program->uniform("iM", model);
    program->uniform("iMV", model**view);
    if (renderPass==CAMERA) {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, checkerboardTexture->color());
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, checkerboardTexture->normal());
    }
    program->drawCube();
    program->uniform("iNormalFlip", 1.0f);

    if (renderPass==CAMERA) {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, plainColorTexture->color());
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, checkerboardTexture->normal());
    }

    model.scale(float3(10,10,10));
    r.rotX(_time*0.37); model = model*r;
    r.rotY(_time*0.13); model = model*r;
    r.rotZ(_time*0.26); model = model*r;
    translate.translate(float3(-15,-12,0));
    model = model*translate;
    program->uniform("iM", model);
    program->uniform("iMV", model**view);
    program->drawCube();

    model.scale(float3(10,10,10));
    r.rotX(_time*0.17); model = model*r;
    r.rotY(_time*0.53); model = model*r;
    r.rotZ(_time*0.96); model = model*r;
    translate.translate(float3(15,-12,0));
    model = model*translate;
    program->uniform("iM", model);
    program->uniform("iMV", model**view);
    program->drawCube();
}

void Engine::render(RenderTarget& frontBuffer) {
    _time = glfwGetTime();

    matrix4 projection = perspectiveFOV(M_PI/5, ((float)frontBuffer.width())/((float)frontBuffer.height()), 0.1);
    //matrix4 view = lookAt(float3(sin(_time*0.3)*100, 50+sin(_time*2.12)*10, cos(_time*0.3)*100), float3(0,0,0), float3(0,1,0));
    //matrix4 view = lookAt(float3(80, 40, 80), float3(0,-20,0), float3(0,1,0));
    //matrix4 view = lookAt(float3(0, 100, 0), float3(0,0,0), float3(1,0,0));
   
    matrix4 view = lookAt(float3(sin(_rotX)*100, sin(_rotY)*50+50, cos(_rotX)*100), float3(0,0,0), float3(0,1,0));
    
  //  view = reflect(float4(0,1,0,-100))*view;
//    glCullFace(GL_FRONT);

 //   shaderToyTexture->update(_time);

    float i=0;
    for (auto lightSource : lightSources) {
        float spread = sin(_time)*0.5+0.5;
        float iv = (M_PI*2.0/((float)lightSources.size()))*i;
        float v = iv*spread + _time*1;
        float r = sin(_time*2.76)*5+40;
        float3 pos = float3(sin(v)*r,-20+fabs(sin(iv+_time*3)*20),cos(v)*r);

        lightSource->setPosition(pos);
        lightSource->generateShadowMap(this);
        i++;
    }

    gBuffer->use();
    renderScene(CAMERA, projection, &view);

    glDisable(GL_DEPTH_TEST);

    ssaoBuffer->use();
    ssaoProgram->use();
    glActiveTexture(GL_TEXTURE0);        
    glBindTexture(GL_TEXTURE_2D, gBuffer->normal());
    glActiveTexture(GL_TEXTURE1);        
    glBindTexture(GL_TEXTURE_2D, gBuffer->position());
    glActiveTexture(GL_TEXTURE2);        
    glBindTexture(GL_TEXTURE_2D, randomTexture->color());
    ssaoProgram->uniform("color", float3(0.3, 0.3, 0.3));
    ssaoProgram->drawQuad();

    accumulateBuffer->use();
    ambientProgram->use();
    glActiveTexture(GL_TEXTURE0);        
    glBindTexture(GL_TEXTURE_2D, gBuffer->color());
    glActiveTexture(GL_TEXTURE1);        
    glBindTexture(GL_TEXTURE_2D, ssaoBuffer->color());
    ambientProgram->drawQuad();

    glBlendFunc(GL_ONE, GL_ONE);
    glEnable(GL_BLEND);

    omnilightProgram->use();
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, gBuffer->color());
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, gBuffer->normal());
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, gBuffer->position());
    for (LightSource* lightSource : lightSources) {
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_CUBE_MAP, lightSource->shadowMap());
        omnilightProgram->uniform("lightPositionVS", ~view*lightSource->position());
        omnilightProgram->uniform("iInvViewRot", !view, true);
        omnilightProgram->uniform("lightColor", lightSource->getColor() * lightSource->getIntensity());
        omnilightProgram->drawQuad();
    }

    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_FALSE);
    whiteProgram->use();
    whiteProgram->uniform("iP", projection);
    matrix4 scale;
    scale.scale(float3(2, 2, 2));
    matrix4 translate;
    for (LightSource* lightSource : lightSources) {
        translate.translate(lightSource->position());
        whiteProgram->uniform("iMV", scale*translate*view);
        whiteProgram->uniform("color", lightSource->getColor());
        whiteProgram->drawCube();
    }
    glDepthMask(GL_TRUE);
    glDisable(GL_BLEND);
    glDisable(GL_DEPTH_TEST);

    frontBuffer.use();
    dof->use();
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, accumulateBuffer->color());
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, gBuffer->position());
    dof->drawQuad();
/*
    visualizeCube->use();
    glBindTexture(GL_TEXTURE_CUBE_MAP, lightSource->depthMap());
    visualizeCube->drawCubeTexture();
*/
return;
    // Slow blitting but useful for MSAA
/*
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    glBindFramebuffer(GL_READ_FRAMEBUFFER, gBuffer->renderTarget()->frameBuffer());
    glDrawBuffer(GL_BACK);
    glBlitFramebuffer(0, 0, gBuffer->width(), gBuffer->height(), 0, 0, frontBuffer.width(), frontBuffer.height(), GL_COLOR_BUFFER_BIT, GL_NEAREST);
*/

    /*
return;
    visualizeCube->use();
    glBindTexture(GL_TEXTURE_CUBE_MAP, lightSource->shadowMap());
    visualizeCube->drawCubeTexture();*/
}
