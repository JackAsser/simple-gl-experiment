#pragma once

// The basic set of headers we need.
// Header rule 101: Don't pull in more than you need to.
// In particular, don't include stdio.h, windows.h or STL in headers
// that get used everywhere. Your compilation speed will skyrocket.
#include <stdint.h>
#include <math.h>
#include <xmmintrin.h>

// If you want it inlined, inline it.
// If you don't want it inlined, don't inline it.
// There is no in-between. Make your mind up :)
#define VM_INLINE	inline __vectorcall
#define FLT_MAX     3.402823466e+38F

// Helpers for initializing static data.
#define VCONST extern const /*__declspec(selectany)*/
struct vconstu
{
	union { uint32_t u[4]; __m128 v; };
	inline operator __m128() const { return v; }
};

VCONST vconstu vsignbits;

#include "float3.h"
#include "float4.h"
#include "matrix4.h"
