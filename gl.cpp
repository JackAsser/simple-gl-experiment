#include "gl.h"
#include <stdio.h>
#include <stdlib.h>

GLuint createTexture(GLenum target, GLenum wrapS, GLenum wrapT, GLenum magFilter, GLenum minFilter) {
	GLuint id;

	glGenTextures(1, &id);
    glBindTexture(target, id);
    glTexParameteri(target, GL_TEXTURE_WRAP_S, wrapS);
    glTexParameteri(target, GL_TEXTURE_WRAP_T, wrapT);
    glTexParameteri(target, GL_TEXTURE_MAG_FILTER, magFilter);
    glTexParameteri(target, GL_TEXTURE_MIN_FILTER, minFilter);

    return id;
}
