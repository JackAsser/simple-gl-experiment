#pragma once

#include <map>
#include "vmath.h"
#include "shader.h"

class Program {
private:
	std::string _name;
	std::map<std::string, GLint> _uniformLocations;
	std::map<std::string, GLint> _attributeLocations;
	GLuint _index;
	GLint getUniformLocation(std::string& name);
	GLint getAttributeLocation(std::string& name);
	GLuint _quadVAO;
	GLuint _cubeVAO;
	GLuint _cubeTextureVAO;
	static GLuint quadVBO;
	static GLuint cubeVBO;
	static GLuint cubeIndexVBO;
	static GLuint cubeTextureVBO;

public:
	Program(std::string name);
	virtual ~Program();

	void attachShader(Shader& shader);
	void link();
	void use();
	void uniform(std::string name, float x, float y);
	void uniform(std::string name, int value);
	void uniform(std::string name, float value);
	void uniform(std::string name, double value);
	void uniform(std::string name, float3 value);
	void uniform(std::string name, float4 value);
	void uniform(std::string name, matrix4 value, bool onlyTopLeft=false);
	void uniform(std::string name, int count, matrix4* values);
	void attribPointer(std::string name, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid* ptr);

	void drawQuad();
	void drawCube();
	void drawCubeTexture();
};
