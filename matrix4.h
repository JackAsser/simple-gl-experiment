#pragma once

#include "util.h"

struct matrix4 {
	VM_INLINE matrix4() {}

	VM_INLINE explicit matrix4(float4 r0, float4 r1, float4 r2, float4 r3) {r[0]=r0; r[1]=r1; r[2]=r2; r[3]=r3;}

	void identity() {
		r[0] = float4(1,0,0,0);
		r[1] = float4(0,1,0,0);
		r[2] = float4(0,0,1,0);
		r[3] = float4(0,0,0,1);		
	}

	VM_INLINE void scale(float3 s) {
		r[0] = float4(s[0], 0, 0, 0);
		r[1] = float4(0, s[1], 0, 0);
		r[2] = float4(0, 0, s[2], 0);
		r[3] = float4(0, 0, 0, 1);
	}

	VM_INLINE void translate(float3 s) {
		r[0] = float4(1, 0, 0, 0);
		r[1] = float4(0, 1, 0, 0);
		r[2] = float4(0, 0, 1, 0);
		r[3] = float4(s[0],s[1],s[2],1);
	}

	void rotX(float a) {
		float c = cosf(a);
		float s = sinf(a);
		r[0] = float4(1, 0, 0, 0);
		r[1] = float4(0, c, s, 0);
		r[2] = float4(0,-s, c, 0);
		r[3] = float4(0, 0, 0, 1);
	}

	void rotY(float a) {
		float c = cosf(a);
		float s = sinf(a);
		r[0] = float4(c, 0, s, 0);
		r[1] = float4(0, 1, 0, 0);
		r[2] = float4(-s,0, c, 0);
		r[3] = float4(0, 0, 0, 1);
	}

	void rotZ(float a) {
		float c = cosf(a);
		float s = sinf(a);
		r[0] = float4(c, s, 0, 0);
		r[1] = float4(-s, c,0, 0);
		r[2] = float4(0, 0, 1, 0);
		r[3] = float4(0, 0, 0, 1);
	}
	
	VM_INLINE void store(float *p) const {
		r[0].store(p+0);
		r[1].store(p+4);
		r[2].store(p+8);
		r[3].store(p+12);
	}

	VM_INLINE float4 c(int i) {
		return float4(r[0][i], r[1][i], r[2][i], r[3][i]);
	}

	// Inverse
	VM_INLINE matrix4 operator!() {
		double det,pos,neg,temp;

		pos = neg = 0;
	    temp = r[0][0] * r[1][1] * r[2][2];
	    if (temp>=0) pos+=temp; else neg+=temp;
	    temp = r[1][0] * r[3][2] * r[0][2];
	    if (temp>=0) pos+=temp; else neg =temp;
	    temp = r[2][0] * r[0][1] * r[1][2];
	    if (temp>=0) pos+=temp; else neg =temp;
	    temp = -r[2][0] * r[1][1] * r[0][2];
	    if (temp>=0) pos+=temp; else neg =temp;
	    temp = -r[1][0] * r[0][1] * r[2][2];
	    if (temp>=0) pos+=temp; else neg =temp;
	    temp = -r[0][0] * r[2][1] * r[1][2];
	    if (temp>=0) pos+=temp; else neg =temp;
	    det = pos + neg;

	    if ((det==0.0) || (fabs(det / (pos - neg)) < 1.0e-15))
			bail("Matrix is not singular, inverse not possible! det:%f, pos:%f, neg:%f", det, pos, neg);

		det = 1.0/det;

		matrix4 inv(
			 float4(  ( r[1][1] * r[2][2] - r[2][1] * r[1][2] )*det,
					- ( r[0][1] * r[2][2] - r[2][1] * r[0][2] )*det,
					  ( r[0][1] * r[1][2] - r[1][1] * r[0][2] )*det,
					0),
			 float4(- ( r[1][0] * r[2][2] - r[2][0] * r[1][2] )*det,
			 		  ( r[0][0] * r[2][2] - r[2][0] * r[0][2] )*det,
			 		- ( r[0][0] * r[1][2] - r[1][0] * r[0][2] )*det,
			 		0),
			 float4(  ( r[1][0] * r[2][1] - r[2][0] * r[1][1] )*det,
			 		- ( r[0][0] * r[2][1] - r[2][0] * r[0][1] )*det,
			 		  ( r[0][0] * r[1][1] - r[1][0] * r[0][1] )*det,
			 		0),
			 float4(0,0,0,1)
		);

		inv.r[3] = float4(- ( r[3][0] * inv[0][0] + r[3][1] * inv[1][0] + r[3][2] * inv[2][0] ),
						  - ( r[3][0] * inv[0][1] + r[3][1] * inv[1][1] + r[3][2] * inv[2][1] ),
						  - ( r[3][0] * inv[0][2] + r[3][1] * inv[1][2] + r[3][2] * inv[2][2] ),
						  1);

		return inv;
	}

	// Transpose
	VM_INLINE matrix4 operator~() {
		return matrix4(c(0), c(1), c(2), c(3));
	}

	VM_INLINE float4 operator[] (size_t i) const { return r[i]; };

	void dump() {
		for (int i=0; i<4; i++)
			r[i].dump();
	}

	float4 r[4];
};

VM_INLINE float4 operator* (float4 a, matrix4 b) {
	return float4(dot(a,b[0]), dot(a,b[1]), dot(a,b[2]), dot(a,b[3]));
}

VM_INLINE float4 operator* (matrix4 b, float4 a) {
	return float4(dot(a,b[0]), dot(a,b[1]), dot(a,b[2]), dot(a,b[3]));
}

VM_INLINE float3 operator* (float3 a, matrix4 b) {
	float4 tmp=float4(a.x(), a.y(), a.z(), 1.0f)*b;
	return float3(tmp.x(), tmp.y(), tmp.z());
}

VM_INLINE float3 operator* (matrix4 b, float3 a) {
	float4 tmp=float4(a.x(), a.y(), a.z(), 1.0f)*b;
	return float3(tmp.x(), tmp.y(), tmp.z());
}

VM_INLINE matrix4 operator* (matrix4 a, matrix4 b) {
	b = ~b;
	return matrix4(
		float4(dot(a.r[0],b.r[0]), dot(a.r[0],b.r[1]), dot(a.r[0],b.r[2]), dot(a.r[0],b.r[3])), 
		float4(dot(a.r[1],b.r[0]), dot(a.r[1],b.r[1]), dot(a.r[1],b.r[2]), dot(a.r[1],b.r[3])), 
		float4(dot(a.r[2],b.r[0]), dot(a.r[2],b.r[1]), dot(a.r[2],b.r[2]), dot(a.r[2],b.r[3])), 
		float4(dot(a.r[3],b.r[0]), dot(a.r[3],b.r[1]), dot(a.r[3],b.r[2]), dot(a.r[3],b.r[3]))
	);
}

VM_INLINE matrix4 lookAt(float3 eye, float3 at, float3 up) {
	float3 f = normalize(at-eye);
	float3 upActual = normalize(up);
	float3 s = cross(f, upActual);
	float3 u = cross(s, f);
	
	matrix4 tmp = matrix4(
		float4(s.x(), u.x(), -f.x(), 0),
		float4(s.y(), u.y(), -f.y(), 0),
		float4(s.z(), u.z(), -f.z(), 0),
		float4(0, 0, 0, 1)
	);

	matrix4 t;
	t.translate(-eye);

	return t*tmp;
}

VM_INLINE matrix4 perspectiveFOV(float FOVy, float aspect, float near, float far, bool rotate) {
	aspect = rotate?1.0/aspect:aspect;

	float f = 1.0f / tanf(FOVy * 0.5f);
	float n = 1.0f / (near - far);

	matrix4 tmp = matrix4(
		float4(f/aspect, 0,0,0),
		float4(0,f,0,0),
		float4(0,0,(far+near)*n,-1),
		float4(0,0,(2*far*near)*n,0)
	);

	if (rotate)	{
		matrix4 r;
		r.rotZ(-90.0f*M_PI/180.0f);
		return tmp*r;
	}

	return tmp;
}

VM_INLINE matrix4 perspectiveFOV(float FOVy, float aspect, float near) {
	float f = 1.0f / tanf(FOVy * 0.5f);

	return matrix4(
		float4(f/aspect, 0,0,0),
		float4(0,f,0,0),
		float4(0,0,0,-1),
		float4(0,0,near,0)
	);
}

// DynamicParams.m_mViewProj = mReflect * mViewProj;
VM_INLINE matrix4 reflect(float4 mirrorPlane) {
	float a = mirrorPlane.x();
	float b = mirrorPlane.y();
	float c = mirrorPlane.z();
	float d = mirrorPlane.w();
	return matrix4(
		float4(-2*a*a+1, -2*b*a,   -2*c*a,   0),
		float4(-2*a*b,   -2*b*b+1, -2*c*b,   0),
		float4(-2*a*c,   -2*b*c,   -2*c*c+1, 0),
		float4(-2*a*d,   -2*b*d,   -2*c*d,   1)
	);
}

/*

1) p=[x,y,z,1]
2) p´ = p*projectionMatrix = [x*f/aspect, y*f, near, -z]
3) p´´ = p´/p´.w = [-(x*f/aspect)/z, -(y*f)/z, -near/z, 1]
4) p´´´ = p´´*2.0-1.0

*/
