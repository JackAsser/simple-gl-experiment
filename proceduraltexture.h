#pragma once

#include <string>
#include "gbuffer.h"
#include "program.h"

class ProceduralTexture : public GBuffer {
private:
	Program* p;

public:
	ProceduralTexture(int size, GBufferMask mask, std::string filename);
	virtual ~ProceduralTexture();
	void update(double time=0);
	Program* getProgram();
};
